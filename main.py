from flask import Flask, render_template, request, send_from_directory, redirect, url_for
import os
import zipfile

app = Flask(__name__)

UPLOAD_FOLDER = 'upload'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/', methods=['GET', 'POST'])
def upload_files():
    if request.method == 'POST':
        numero_iniziale = request.form.get('numero')
        if 'files[]' not in request.files:
            return 'Nessun file selezionato'

        files = request.files.getlist('files[]')

        converted_images = []
        for index, file in enumerate(files, start=int(numero_iniziale)):
            file_extension = os.path.splitext(file.filename)[1]
            new_filename = f'{index:03d}{file_extension}'
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], new_filename))
            converted_images.append(new_filename)

        zip_filename = 'converted_images.zip'
        with zipfile.ZipFile(zip_filename, 'w') as zipf:
            for image_file in converted_images:
                zipf.write(os.path.join(app.config['UPLOAD_FOLDER'], image_file), image_file)

        return render_template('upload.html', download_link=url_for('download_file', filename=zip_filename), images=converted_images)

    return render_template('upload.html')


@app.route('/download/<filename>')
def download_file(filename):
    return send_from_directory('.', filename, as_attachment=True)


@app.route('/delete_images', methods=['GET', 'POST'])
def delete_images():
    if request.method == 'POST':
        # Elimina le immagini nella cartella "upload"
        for filename in os.listdir('upload'):
            file_path = os.path.join('upload', filename)
            os.remove(file_path)

        return redirect('/')

    return render_template('delete_images.html')


if __name__ == '__main__':
    app.run(debug=True)
